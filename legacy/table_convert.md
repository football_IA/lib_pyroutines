acosd: np.arccos
butter:signal.butter
cosd: np.degrees
dot: np.dot
find:np.argwhere
filtfilt:signal.filtfilt
interp1:interpolate.interp1d
linspace:np.linspace
norm: np.linalg.norm
size:np.shape
sind:np.sin(np.radians(N))
zeros:np.zeros
pca:StandardScaler().fit_transform(x)
': np.matrix or [:, np.newaxis]
[;]: np.vstack



operators:
.*:*
.^:**

Arrays

# no python pode ser usado o np.vstack((a, b))
[lz;file] # insere o array lz no inicio do array
[file;lz] # o ; faz uma inserção no final do array
[file;lz] # a , faz uma concatenação entre os arrays

>ave =    
>> 0.7404    0.2727
>> -0.0559   0.2716
>>  0.0044    0.8781
>> -0.6699    0.2845

>>ave(2,1)        // (row, column)
>>   -0.0559


exemplo de uso do comando (:,1), pengando somente os 
primeiros numeros da cada array (ex: in numpy [:,0])
A = [1 2 3; 4 5 6];

>> A(:,1)
ans =
     1
     4

>> X = A(:,1)'
X =







referencias:
http://scipy.github.io/old-wiki/pages/NumPy_for_Matlab_Users
https://numpy.org/devdocs/user/numpy-for-matlab-users.html
https://www.javatpoint.com/numpy-mean
http://mathesaurus.sourceforge.net/matlab-numpy.html