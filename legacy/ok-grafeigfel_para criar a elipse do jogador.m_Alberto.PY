# %% A partir de dados x e y do jogador, retorna o comprimento dos eixos e a �rea
# %% da elipse formada
#
#
# function [comp1,comp2,area]=grafeigfel(coorda)
#
#
# %
# %coorda=coord(:,nj*2:nj*2+1);
# [ave,~,ava]=pca(coorda);
# sava2=sqrt(ava(2,1));
# sava1=sqrt(ava(1,1));
# comp1=sava1;
# comp2=sava2;
# %ave=[ave(:,1) ave(:,2)*-1];
# area=pi*sava1*sava2;
# ave1=ave(:,1);
# ave2=ave(:,2);
#
# mx=mean(coorda(:,1));
# my=mean(coorda(:,2));
# campo
# %subplot(212);
# %plot(x,y,'+k');hold on;plot(mx,my,'ok');
# %campo
# %daspect([1 1 1]);
# %plot(coorda(:,1),coorda(:,2),'b.');
#
# %texto=text(mx-1,my-1.5,num2str(nj),'FontSize',12);
# %set(texto,'Color','r');
# %set(texto,'FontWeight','bold');
#
#
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# % Criando elipse
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#
# if ave(2,1)<=0
#    ave(1,1)=ave(1,1)*-1;
#    % rotacao da elipse
#    giro=-acos(ave(1)); %angulo de rotacao da elipse
#    ex=sava1; %eixo x da elipse
# ey=sava2; %eixo y da elipse
# %desenho
# x=(-ex):0.0001:(ex);
# ysup = ey.*sqrt( 1-((x)/ex).^2 );
#
# %rotacao da elipse
# x1= x.*cos(giro) + ysup.*sin(giro);
# x2= x.*cos(-giro) + ysup.*sin(-giro);
# ysup1= -x.*sin(giro) + ysup.*cos(giro);
# yinf1= -x.*sin(giro) - ysup.*cos(giro);
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# %elipse rotacionada
# plot(x1+mx,ysup1+my,'.b','LineWidth',10)
# plot(x2+mx,yinf1+my,'.b','LineWidth',10);
#
# % pause(0.0001);
# hold on
# plot([mx;(-sava2*ave1(2))+mx],[my;(sava2*ave1(1))+my],'k','LineWidth',2)
# plot([mx;(sava1*ave2(2))+mx],[my;(-sava1*ave2(1))+my],'k','LineWidth',2)
# plot([mx;(-sava1*ave2(2))+mx],[my;(sava1*ave2(1))+my],'k','LineWidth',2)
# plot([mx;(sava2*ave1(2))+mx],[my;(-sava2*ave1(1))+my],'k','LineWidth',2)
#
# else giro=-acos(ave(1)); %angulo de rotacao da elipse
# %definicao dos semi-eixos
# ex=sava1; %eixo x da elipse
# ey=sava2; %eixo y da elipse
# %desenho
# x=(-ex):0.0001:(ex);
# ysup = ey.*sqrt( 1-((x)/ex).^2 );
# yinf = -ey.*sqrt( 1-((x)/ex).^2 );

# %rotacao da elipse
# x1= x.*cos(giro) + ysup.*sin(giro);
# x2= x.*cos(-giro) + ysup.*sin(-giro);
# ysup1= -x.*sin(giro) + ysup.*cos(giro);
# yinf1= -x.*sin(giro) - ysup.*cos(giro);
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# %elipse rotacionada
# plot(x1+mx,ysup1+my,'.b','LineWidth',10)
# plot(x2+mx,yinf1+my,'.b','LineWidth',10);
# % pause(0.0001);
#
# hold on
# plot([mx;(-sava2*ave1(2))+mx],[my;(sava2*ave1(1))+my],'k','LineWidth',2)
# plot([mx;(sava1*ave2(2))+mx],[my;(-sava1*ave2(1))+my],'k','LineWidth',2)
# plot([mx;(-sava1*ave2(2))+mx],[my;(sava1*ave2(1))+my],'k','LineWidth',2)
# plot([mx;(sava2*ave1(2))+mx],[my;(-sava2*ave1(1))+my],'k','LineWidth',2)
# end
#
# %
# % %% Para Time B
# % coordb=coord(:,3:4);
# % [ave,sco,ava]=princomp(coordb);
# % sava2=sqrt(ava(2,1));
# % sava1=sqrt(ava(1,1));
# %
# % %ave=[ave(:,1) ave(:,2)*-1];
# %
# % ave1=ave(:,1);
# % ave2=ave(:,2);
# %
# % mx=mean(coordb(:,1));
# % my=mean(coordb(:,2));
# %
# % %subplot(212);
# % %plot(x,y,'+k');hold on;plot(mx,my,'ok');
# % camposer([106 72])
# % daspect([1 1 1]);
# % plot(coordb(:,1),coordb(:,2),'r*');
# % hold on
# % plot([mx;(-sava2*ave1(2))+mx],[my;(sava2*ave1(1))+my],'r','LineWidth',2)
# % plot([mx;(sava1*ave2(2))+mx],[my;(-sava1*ave2(1))+my],'r','LineWidth',2)
# % plot([mx;(-sava1*ave2(2))+mx],[my;(sava1*ave2(1))+my],'r','LineWidth',2)
# % plot([mx;(sava2*ave1(2))+mx],[my;(-sava2*ave1(1))+my],'r','LineWidth',2)
# % % texto=text(mx-1,my-1.5,num2str(nj),'FontSize',12);
# % % set(texto,'Color','r');
# % % set(texto,'FontWeight','bold');
# %
# %
# % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# % % Criando elipse
# % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# %
# % if ave(2,1)<=0;
# %    ave(1,1)=ave(1,1)*-1;
# %    % rotacao da elipse
# %    giro=-acos(ave(1)); %angulo de rotacao da elipse
# %    ex=sava1; %eixo x da elipse
# % ey=sava2; %eixo y da elipse
# % %desenho
# % x=(-ex):0.0001:(ex);
# % ysup = ey.*sqrt( 1-((x)/ex).^2 );
# % yinf = -ey.*sqrt( 1-((x)/ex).^2 );
# % %rotacao da elipse
# % x1= x.*cos(giro) + ysup.*sin(giro);
# % x2= x.*cos(-giro) + ysup.*sin(-giro);
# % ysup1= -x.*sin(giro) + ysup.*cos(giro);
# % yinf1= -x.*sin(giro) - ysup.*cos(giro);
# % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# % %elipse rotacionada
# % plot(x1+mx,ysup1+my,'r')
# % plot(x2+mx,yinf1+my,'r');
# % hold off
# % % pause(0.0001);
# %
# % else giro=-acos(ave(1)); %angulo de rotacao da elipse
# % %definicao dos semi-eixos
# % ex=sava1; %eixo x da elipse
# % ey=sava2; %eixo y da elipse
# % %desenho
# % x=(-ex):0.0001:(ex);
# % ysup = ey.*sqrt( 1-((x)/ex).^2 );
# % yinf = -ey.*sqrt( 1-((x)/ex).^2 );
# % %rotacao da elipse
# % x1= x.*cos(giro) + ysup.*sin(giro);
# % x2= x.*cos(-giro) + ysup.*sin(-giro);
# % ysup1= -x.*sin(giro) + ysup.*cos(giro);
# % yinf1= -x.*sin(giro) - ysup.*cos(giro);
# % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# % %elipse rotacionada
# % plot(x1+mx,ysup1+my,'r')
# % plot(x2+mx,yinf1+my,'r');
# % hold off
# % % pause(0.0001);
# % end
# %hold off
# end


import numpy as np
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import math


def grafeigfel(coorda):
    comp1, comp2, area = None

    ave, ava = StandardScaler().fit_transform(coorda)
    sava1 = math.sqrt(np.array(ava)[0, 0])
    sava2 = math.sqrt(np.array(ava)[1, 0])
    comp1 = sava1
    comp2 = sava2
    area = math.pi * sava1 * sava2
    ave1 = np.array(ave)[:, 1]  # pega a primeira coluna do array
    ave2 = np.array(ave)[:, 2]  # pega a segunda coluna do array

    mx = np.mean(np.array(coorda)[:, 1])  # media da primeira coluna do coorda
    my = np.mean(np.array(coorda)[:, 2])  # media da segunda coluna do coorda

    if np.array(ave)[1, 0] <= 0:
        # [row,column] anote
        # ex: [[1,2,3],
        #     [10,12,22]]
        # [1,0] filtra pela row 2 e pega o 1 elemto do array
        np.array(ave)[1, 1] = np.array(ave)[1, 1] * -1  # converte de positivo para negativo
        # rotacao da elipse
        giro = float(str(-math.acos(np.array(ave)[0, 0]))[:7])  # angulo de rotacao da elipse
        ex = sava1  # eixo x da elipse

    ey = sava2  # eixo y da elipse
    # desenho
    # x=(-ex):0.0001:(ex);
    x = np.arange(start=-ex, stop=ex, step=0.0001)
    ysup = ey * np.sqrt(1 - ((x) / ex) ** 2)

    # rotacao da elipse
    x1 = x * math.cos(giro) + ysup * math.sin(giro)
    x2 = x * math.cos(-giro) + ysup * math.sin(-giro)
    ysup1 = -x * math.sin(giro) + ysup * math.cos(giro)
    yinf1 = -x * math.sin(giro) - ysup * math.cos(giro)

    # elipse rotacionada
    plt.plot(x1 + mx, ysup1 + my, '.b', 'LineWidth', 10)
    plt.plot(x2 + mx, yinf1 + my, '.b', 'LineWidth', 10)
    plt.show()

    # pause(0.0001);
    # hold on
    plt.plot([mx, (-sava2 * ave1(2)) + mx], [my, (sava2 * ave1(1)) + my], 'k', 'LineWidth', 2)
    plt.plot([mx, (sava1 * ave2(2)) + mx], [my, (-sava1 * ave2(1)) + my], 'k', 'LineWidth', 2)
    plt.plot([mx, (-sava1 * ave2(2)) + mx], [my, (sava1 * ave2(1)) + my], 'k', 'LineWidth', 2)
    plt.plot([mx, (sava2 * ave1(2)) + mx], [my, (-sava2 * ave1(1)) + my], 'k', 'LineWidth', 2)

    giro = -math.acos(ave(1));  # angulo de rotacao da elipse
    # definicao dos semi-eixos
    ex = sava1;  # eixo x da elipse
    ey = sava2;  # eixo y da elipse

    # desenho
    # x=(-ex):0.0001:(ex);
    x = np.arange(start=-ex, stop=ex, step=0.0001)
    ysup = ey * np.sqrt(1 - ((x) / ex) ** 2)
    yinf = -ey * np.sqrt(1 - ((x) / ex) ** 2)

    # rotacao da elipse
    x1 = x * math.cos(giro) + ysup * math.sin(giro)
    x2 = x * math.cos(-giro) + ysup * math.sin(-giro)
    ysup1 = -x * math.sin(giro) + ysup * math.cos(giro)
    yinf1 = -x * math.sin(giro) - ysup * math.cos(giro)

    # elipse rotacionada
    plt.plot(x1 + mx, ysup1 + my, '.b', 'LineWidth', 10)
    plt.plot(x2 + mx, yinf1 + my, '.b', 'LineWidth', 10)

    # hold on
    plt.plot([mx, (-sava2 * ave1(2)) + mx], [my, (sava2 * ave1(1)) + my], 'k', 'LineWidth', 2)
    plt.plot([mx, (sava1 * ave2(2)) + mx], [my, (-sava1 * ave2(1)) + my], 'k', 'LineWidth', 2)
    plt.plot([mx, (-sava1 * ave2(2)) + mx], [my, (sava1 * ave2(1)) + my], 'k', 'LineWidth', 2)
    plt.plot([mx, (sava2 * ave1(2)) + mx], [my, (-sava2 * ave1(1)) + my], 'k', 'LineWidth', 2)
    # end
