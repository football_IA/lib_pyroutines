import math

import numpy as np
import matplotlib.pyplot as plt

a=np.array([[12,45,45],
            [45,22,12]])
# a=np.array([1,2,3, 5])
# b=np.array([10,12,22, 33])
# num_rows, num_cols = a.shape
# num_rows= a.shape[0]
# print(num_rows, num_cols)
# print(a[:,0] - 5)
# print(a[1,0])
# b = b * 2
# print(b)
# plt.plot(a,b)
# plt.show()

lon_ref = a
lat_ref = a*a

cosseno = math.cos(50)
seno = math.sin(90)

coorspheric_rodado = []
for i, v in enumerate(np.shape(a)):
    i += 1
    for j in range(2):
        coorspheric_rodado = np.matrix([[cosseno, seno], [-seno, cosseno]])

        print(coorspheric_rodado)