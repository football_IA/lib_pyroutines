"""
% Essa rotina calcula a dist�ncia e velocidade dos jogadores tempo a tempo
% atrav�s de subtra��o de matrizes.



function [matVel,matDist]=distanvel(file,freq);
[nLin, nCol]=size(file);


linhazero=zeros(1,nCol);
filea=[file;linhazero];
fileb=[linhazero;file];


matDist= [sqrt([(fileb(:,1:2:nCol)-filea(:,1:2:nCol)).^2] + [(fileb(:,2:2:nCol)-filea(:,2:2:nCol)).^2])];

[m,n]=find(matDist>10);
nlin_m=size(m,1);

for i=1:nlin_m;
    matDist(m(i),n(i))=0;
end

nlin_matdist= size(matDist,1);
tempo=[1:nLin-1]';
matDist=matDist(2:nlin_matdist-1,:);

matVel=[tempo matDist/(1/freq)];
matDist=[tempo matDist];"""


#python3
import pandas as pd
import numpy as np
import math
def distanvel(file,freq):
    nLin, nCol = np.shape(file)
    linhazero = np.zeros(len(nCol)-1,1)
	filea=[file,linhazero];
	fileb=[linhazero,file];
	
	
	matDist= [math.sqrt([(fileb[:,1:2:nCol]-filea[:,1:2:nCol]) ** 2] + [(fileb[:,2:2:nCol]-filea[:,2:2:nCol]) ** 2])];

	[m,n] = np.argwhere(matDist>10) #find() in matlab
	nlin_m = np.shape(m,1)
	
	for i in range(len(nlin_m)):
		matDist[m[i],n[i]] = 0
    
	nlin_matdist= np.shape(matDist,1)
    tempo = np.matrix(1:nLin-1)
    matDist=matDist[2:nlin_matdist-1,:]
    
    matVel=[tempo, matDist / ( 1/ freq) ]
    matDist=[tempo, matDist]

    distVel = np.array(matVel,matDist)

    return distVel
    
 #90% CONCLUIDO