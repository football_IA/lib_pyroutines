'''
%% Essa rotina converte dados de GPS para o plano cartesiano
%% Entre em dados com uma matriz com duas colunas representando a latitude e 
%% longitude do atleta
%% lat_o e long_o representam o escanteio de origem
%% lat2 e long2 representam o escanteio do mesmo lado da origem, em comprimento
%% lat3 e long3 representam o escanteio oposto, formando a diagonal 


function [coord2d] = gps2cart(dados,lat_o,long_o,lat2,long2,lat3,long3);
  
%% Coloca a latitude e longitude do jogador no sistema de refer�ncias do
%% campo (ainda em lat e longitude);
%Portanto:

lat_ref=dados(:,1)-(lat_o);
lon_ref=dados(:,2)-(long_o);

%% Calula latitude e longitude os escanteios, ajustados na origem [0 0];

lat_escanteio2=lat2-(lat_o);
lon_escanteio2=long2-(long_o);

lat_escanteio3=lat3-(lat_o); 
lon_escanteio3=long3-(long_o);

% Assim:

%% Roda o vetor latitude e longitude para o plano ficar perpendicular aos
%% eixos x e y.

vetorpos_escanteio2=[lon_escanteio2 lat_escanteio2];
vetorpos_escanteio3=[lon_escanteio3 lat_escanteio3];
vetor_horizontal=[1 0];

angulo=acosd(dot(vetorpos_escanteio2/norm(vetorpos_escanteio2),vetor_horizontal/norm(vetor_horizontal)));

cosseno=cosd(angulo);
seno=sind(angulo);

vetorpos_escanteio2rodado=[cosseno seno;-seno cosseno]*vetorpos_escanteio2';
vetorpos_escanteio2rodado=vetorpos_escanteio2rodado';

vetorpos_escanteio3rodado=[cosseno seno;-seno cosseno]*vetorpos_escanteio3';
vetorpos_escanteio3rodado=vetorpos_escanteio3rodado';


% plot(0,0,'*');
% hold on;
% plot(vetorpos_escanteio2rodado(1,1),vetorpos_escanteio2rodado(1,2),'k*');
% plot(vetorpos_escanteio3rodado(1,1),vetorpos_escanteio3rodado(1,2),'r*');


for i=1:size(dados,1);
    for j=1:1;
    coorspheric_rodado(i,2*j-1:2*j)=[cosseno seno;-seno cosseno]*[lon_ref(i,j) lat_ref(i,j)]';
    end
end

coordx = (coorspheric_rodado(:,1:2:end).*105)./vetorpos_escanteio3rodado(1,1);
coordy = (coorspheric_rodado(:,2:2:end).*68)./vetorpos_escanteio3rodado(1,2);


coord2d(:,1) = coordx;
coord2d(:,2) = coordy;

  
end
'''


# imports
import numpy as np
def gps2cart(dados, lat_o, long_o, lat2, long2, lat3, long3):
    coord2d = None
    coord2d = np.array(coord2d)
    coorspheric_rodado = None

    lat_ref = dados[:, 0] - lat_o
    lon_ref = dados[:, 1] - long_o

    #  Calcula latitude e longitude os escanteios, ajustados na origem [0 0];

    lat_escanteio2 = lat2 - lat_o
    lon_escanteio2 = long2 - long_o

    lat_escanteio3 = lat3 - lat_o
    lon_escanteio3 = long3 - long_o

    #  Assim:
    #  Roda o vetor latitude e longitude para o plano ficar perpendicular aos
    #  eixos x e y.

    vetorpos_escanteio2 = [lon_escanteio2, lat_escanteio2]
    vetorpos_escanteio3 = [lon_escanteio3, lat_escanteio3]
    vetor_horizontal = [1, 0]


    angulo = np.arccos(np.dot(vetorpos_escanteio2 / np.linalg.norm(vetorpos_escanteio2), vetor_horizontal / np.linalg.norm(vetor_horizontal)))

    cosseno = np.degrees(angulo)
    seno = np.sin(np.radians(angulo))

    vetorpos_escanteio2rodado = [[cosseno, seno],[-seno, cosseno]]*vetorpos_escanteio2
    vetorpos_escanteio2rodado = np.matrix(vetorpos_escanteio2rodado)

    vetorpos_escanteio3rodado = np.matrix([[cosseno, seno],[-seno, cosseno]]*vetorpos_escanteio3)
    vetorpos_escanteio3rodado = np.matrix(vetorpos_escanteio3rodado)

    # plot(0, 0, '*');
    # hold on;
    # plot(vetorpos_escanteio2rodado(1, 1), vetorpos_escanteio2rodado(1, 2), 'k*');
    # plot(vetorpos_escanteio3rodado(1, 1), vetorpos_escanteio3rodado(1, 2), 'r*');

    for i in enumerate(np.shape(dados)):
        for j in range(len(2)):
            np.array(coorspheric_rodado)[i, 2 * j - 1: 2 * j] = np.matrix([[cosseno, seno],[-seno, cosseno]]*[np.array(lon_ref)[i, j], np.array(lat_ref)[i, j]])

    coordx = (coorspheric_rodado[:, 1:2] * 105) / vetorpos_escanteio3rodado(1, 1);
    coordy = (coorspheric_rodado[:, 2:2] * 68) / vetorpos_escanteio3rodado(1, 2);

    coord2d[:, 1] = coordx;
    coord2d[:, 2] = coordy;

    return coord2d
