from scipy import interpolate, signal
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import math


class PyRoutines:
    def interpoladat(self, coord2d, npontos):
        datinterp = []

        x = np.shape(coord2d)
        xx = np.linspace(1, np.shape(coord2d, 1), npontos)

        datinterp.push(interpolate.interp1d(x, coord2d[:1], xx))
        datinterp.push(interpolate.interp1d(x, coord2d[:2], xx))

        return datinterp

    def grafeigfel(self, coorda):
        comp1, comp2, area = None

        ave, ava = StandardScaler().fit_transform(coorda)
        sava1 = math.sqrt(np.array(ava)[0, 0])
        sava2 = math.sqrt(np.array(ava)[1, 0])
        comp1 = sava1
        comp2 = sava2
        area = math.pi * sava1 * sava2
        ave1 = np.array(ave)[:, 1]  # pega a primeira coluna do array
        ave2 = np.array(ave)[:, 2]  # pega a segunda coluna do array

        mx = np.mean(np.array(coorda)[:, 1])  # media da primeira coluna do coorda
        my = np.mean(np.array(coorda)[:, 2])  # media da segunda coluna do coorda

        if np.array(ave)[1, 0] <= 0:
            # [row,column] anote
            # ex: [[1,2,3],
            #     [10,12,22]]
            # [1,0] filtra pela row 2 e pega o 1 elemto do array
            np.array(ave)[1, 1] = np.array(ave)[1, 1] * -1  # converte de positivo para negativo
            # rotacao da elipse
            giro = float(str(-math.acos(np.array(ave)[0, 0]))[:7])  # angulo de rotacao da elipse
            ex = sava1  # eixo x da elipse

        ey = sava2  # eixo y da elipse
        # desenho
        # x=(-ex):0.0001:(ex);
        x = np.arange(start=-ex, stop=ex, step=0.0001)
        ysup = ey * np.sqrt(1 - ((x) / ex) ** 2)

        # rotacao da elipse
        x1 = x * math.cos(giro) + ysup * math.sin(giro)
        x2 = x * math.cos(-giro) + ysup * math.sin(-giro)
        ysup1 = -x * math.sin(giro) + ysup * math.cos(giro)
        yinf1 = -x * math.sin(giro) - ysup * math.cos(giro)

        # elipse rotacionada
        plt.plot(x1 + mx, ysup1 + my, '.b', 'LineWidth', 10)
        plt.plot(x2 + mx, yinf1 + my, '.b', 'LineWidth', 10)
        plt.show()

        # pause(0.0001);
        # hold on
        plt.plot([mx, (-sava2 * ave1(2)) + mx], [my, (sava2 * ave1(1)) + my], 'k', 'LineWidth', 2)
        plt.plot([mx, (sava1 * ave2(2)) + mx], [my, (-sava1 * ave2(1)) + my], 'k', 'LineWidth', 2)
        plt.plot([mx, (-sava1 * ave2(2)) + mx], [my, (sava1 * ave2(1)) + my], 'k', 'LineWidth', 2)
        plt.plot([mx, (sava2 * ave1(2)) + mx], [my, (-sava2 * ave1(1)) + my], 'k', 'LineWidth', 2)

        giro = -math.acos(ave(1));  # angulo de rotacao da elipse
        # definicao dos semi-eixos
        ex = sava1;  # eixo x da elipse
        ey = sava2;  # eixo y da elipse

        # desenho
        # x=(-ex):0.0001:(ex);
        x = np.arange(start=-ex, stop=ex, step=0.0001)
        ysup = ey * np.sqrt(1 - ((x) / ex) ** 2)
        yinf = -ey * np.sqrt(1 - ((x) / ex) ** 2)

        # rotacao da elipse
        x1 = x * math.cos(giro) + ysup * math.sin(giro)
        x2 = x * math.cos(-giro) + ysup * math.sin(-giro)
        ysup1 = -x * math.sin(giro) + ysup * math.cos(giro)
        yinf1 = -x * math.sin(giro) - ysup * math.cos(giro)

        # elipse rotacionada
        plt.plot(x1 + mx, ysup1 + my, '.b', 'LineWidth', 10)
        plt.plot(x2 + mx, yinf1 + my, '.b', 'LineWidth', 10)

        # hold on
        plt.plot([mx, (-sava2 * ave1(2)) + mx], [my, (sava2 * ave1(1)) + my], 'k', 'LineWidth', 2)
        plt.plot([mx, (sava1 * ave2(2)) + mx], [my, (-sava1 * ave2(1)) + my], 'k', 'LineWidth', 2)
        plt.plot([mx, (-sava1 * ave2(2)) + mx], [my, (sava1 * ave2(1)) + my], 'k', 'LineWidth', 2)
        plt.plot([mx, (sava2 * ave1(2)) + mx], [my, (-sava2 * ave1(1)) + my], 'k', 'LineWidth', 2)
        # end

    def gps2cart(self ,dados, lat_o, long_o, lat2, long2, lat3, long3):
        coord2d = None
        coord2d = np.array(coord2d)
        coorspheric_rodado = None

        lat_ref = dados[:, 0] - lat_o
        lon_ref = dados[:, 1] - long_o

        #  Calcula latitude e longitude os escanteios, ajustados na origem [0 0];

        lat_escanteio2 = lat2 - lat_o
        lon_escanteio2 = long2 - long_o

        lat_escanteio3 = lat3 - lat_o
        lon_escanteio3 = long3 - long_o

        #  Assim:
        #  Roda o vetor latitude e longitude para o plano ficar perpendicular aos
        #  eixos x e y.

        vetorpos_escanteio2 = [lon_escanteio2, lat_escanteio2]
        vetorpos_escanteio3 = [lon_escanteio3, lat_escanteio3]
        vetor_horizontal = [1, 0]

        angulo = np.arccos(np.dot(vetorpos_escanteio2 / np.linalg.norm(vetorpos_escanteio2), vetor_horizontal / np.linalg.norm(vetor_horizontal)))

        cosseno = np.degrees(angulo)
        seno = np.sin(np.radians(angulo))

        vetorpos_escanteio2rodado = [[cosseno, seno], [-seno, cosseno]] * vetorpos_escanteio2
        vetorpos_escanteio2rodado = np.matrix(vetorpos_escanteio2rodado)

        vetorpos_escanteio3rodado = np.matrix([[cosseno, seno], [-seno, cosseno]] * vetorpos_escanteio3)
        vetorpos_escanteio3rodado = np.matrix(vetorpos_escanteio3rodado)

        # plot(0, 0, '*');
        # hold on;
        # plot(vetorpos_escanteio2rodado(1, 1), vetorpos_escanteio2rodado(1, 2), 'k*');
        # plot(vetorpos_escanteio3rodado(1, 1), vetorpos_escanteio3rodado(1, 2), 'r*');

        for i in enumerate(np.shape(dados)):
            for j in range(len(2)):
                np.array(coorspheric_rodado)[i, 2 * j - 1: 2 * j] = np.matrix([[cosseno, seno], [-seno, cosseno]] * [np.array(lon_ref)[i, j], np.array(lat_ref)[i, j]])

        coordx = (coorspheric_rodado[:, 1:2] * 105) / vetorpos_escanteio3rodado(1, 1);
        coordy = (coorspheric_rodado[:, 2:2] * 68) / vetorpos_escanteio3rodado(1, 2);

        coord2d[:, 1] = coordx;
        coord2d[:, 2] = coordy;

        return coord2d

    def filtraDados(self,coord2d, freqAq, freqc):
        datsuav = []
        [B, A] = signal.butter(3, freqc / (freqAq / 2))
        datsuav[0] = signal.filtfilt(B, A, coord2d[::1])
        datsuav[1] = signal.filtfilt(B, A, coord2d[::2])

        return datsuav

    # old name distanvel
    def distanciaVelocidade(self, file, freq):
        # np.shape retorna uma tupla com dois valores
        # o 1º é o numeros de linhas
        # 0 2º são o numeros de colunas
        [nLin, nCol] = np.shape(file)

        # np.zeros cria um array de zeros
        linhazero = np.zeros(nCol, dtype=np.int8)

        # np.vstack faz uma concatenação vertical
        filea = np.vstack((file, linhazero))
        fileb = np.vstack((linhazero, file))

        # 1. pega as primeira e segunda coluna do array e subtri
        # 2. Eleva ao quadrado
        # 3. faz a soma entre o array filea e fileb
        matDist = np.array(np.sqrt((fileb[:,1:nCol:2] - filea[:,1:nCol]) ** 2 + (fileb[:,1:2:nCol] - filea[:,1:2:nCol]) ** 2))

        # verifica no array numeros que sao maiores que 10
        [m, n] = np.argwhere(matDist > 10)  # find() in matlab

        # pega a quantidade de linhas o array m tem
        nlin_m = np.shape(m)

        # Na posicao do do indice sera inserido no array matDist o valor 0
        for i in range(nlin_m):
            matDist[m[i], n[i]] = 0

        nlin_matdist = np.shape(matDist)[0]
        tempo = np.arange(0, l)[:, np.newaxis]
        matDist = matDist[1:nlin_matdist - 1, :]

        # matVel = [tempo, matDist / (1 / freq)]
        # matDist = [tempo, matDist]

        # distVel = np.array(matVel, matDist)

        # return distVel

    # faz uma analise tatica individual
    # old name analisetatica
    def analiseTatica(self):
        pass

    # faz uma analise tatica de uma so equipe
    def analiseTaticaPorEquipe(self):
        pass
